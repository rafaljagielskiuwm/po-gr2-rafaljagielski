package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Kolokwium;
import java.util.Scanner;
public class Zadanie1 {
    public static void main(String[] args) {
        int n = -1;
        int licznik = 0;
        float x;
        Scanner odczyt = new Scanner(System.in);
        System.out.println("Podaj n: ");
        while (n < 0) {
            n = odczyt.nextInt();
        }
        for (int i = 0; i < n; i++) {
            System.out.print("Podaj liczbe: ");
            x = odczyt.nextFloat();
            if (x % 3 == 0) {
                licznik++;
            }
        }
        System.out.println("Liczb podzielnych przez 3 jest: " + licznik);
    }
}
