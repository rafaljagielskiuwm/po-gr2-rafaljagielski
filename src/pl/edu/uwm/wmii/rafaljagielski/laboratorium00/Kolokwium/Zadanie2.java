package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Kolokwium;
import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;
public class Zadanie2 {
    public static void main(String[] args) {
        int n = -1;
        int[] tab;
        int max;
        int licznik;
        Scanner odczyt = new Scanner(System.in);
        while (!((n>=1)&&(n<=200))) {
            System.out.println("Podaj n: ");
            n = odczyt.nextInt();
            if(!((n>=1)&&(n<=200))){
                System.out.println("Podano n z poza zakresu");
            }
        }
        tab=new int[n];
        generuj(tab,n,-372,473);
        System.out.println("Wylosowana tablica:");
        System.out.println(Arrays.toString(tab));
        max=maksymalny(tab);
        System.out.println("Element maksymalny to: "+max);
        licznik=ileMax(tab,max);
        System.out.println("Element maksymalny wystepuje "+licznik+" razy");

    }
    public static void generuj(int tab[],int n, int min,int max){
        Random losowa=new Random();
        for(int i=0;i<n;i++){
            tab[i]=losowa.nextInt((max-min)+1)+min;
        }
    }
    public static int maksymalny(int tab[]){
        int max=tab[0];
        for(int i=0; i<tab.length;i++){
            if(tab[i]>max) max=tab[i];
        }
        return max;
    }
    public static int ileMax(int tab[],int max){
        int licznik=0;
        for(int i=0; i<tab.length;i++){
            if(tab[i]==max) licznik++;
        }
        return licznik;
    }
}
