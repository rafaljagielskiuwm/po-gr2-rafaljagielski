package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Wyklad04;

public class RachunekBankowy {
    private double saldo;
    public static double rocznaStopaProcentowa;

    public RachunekBankowy(double saldo){
        this.saldo=saldo;
    }
    public static void setRocznaStopaProcentowa(double wartosc){
        rocznaStopaProcentowa=wartosc;
    }
    public double obliczMiesieczneOdsetki(){
        return (saldo*rocznaStopaProcentowa)/12;
    }
    public void dodajDoSalda(double wartosc){
        saldo+=wartosc;
    }
    public void wyswietlSaldo(){
        System.out.println(saldo);
    }
}
