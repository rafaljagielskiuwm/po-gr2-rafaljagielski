package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Wyklad04;

public class Zadanie1 {
    public static void main(String[] args){
        RachunekBankowy server1=new RachunekBankowy(2000);
        RachunekBankowy server2=new RachunekBankowy(3000);
        RachunekBankowy.setRocznaStopaProcentowa(0.04);
        server1.dodajDoSalda(server1.obliczMiesieczneOdsetki());
        server2.dodajDoSalda(server2.obliczMiesieczneOdsetki());
        System.out.println("stan salda server1: ");
        server1.wyswietlSaldo();
        System.out.println("stan salda server2: ");
        server2.wyswietlSaldo();
        RachunekBankowy.setRocznaStopaProcentowa(0.05);
        server1.dodajDoSalda(server1.obliczMiesieczneOdsetki());
        server2.dodajDoSalda(server2.obliczMiesieczneOdsetki());
        System.out.println("stan salda server1: ");
        server1.wyswietlSaldo();
        System.out.println("stan salda server2: ");
        server2.wyswietlSaldo();
    }
}
