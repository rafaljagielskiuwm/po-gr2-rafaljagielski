package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Wyklad04;

import java.util.Arrays;

public class IntegerSet {
    public boolean[] tab=new boolean[101];
    public IntegerSet(){
        for(int i=0;i<tab.length;i++){
            tab[i]=false;
        }
    }
    public static IntegerSet union(IntegerSet tab1,IntegerSet tab2){
        IntegerSet wynik=new IntegerSet();
        for(int i=1; i<tab1.tab.length;i++){
            if(tab1.tab[i]|tab2.tab[i]) wynik.tab[i]=true;
        }
        return wynik;
    }
    public static IntegerSet intersection(IntegerSet tab1,IntegerSet tab2){
        IntegerSet wynik=new IntegerSet();
        for(int i=1; i<tab1.tab.length;i++){
            if(tab1.tab[i]&tab2.tab[i]) wynik.tab[i]=true;
        }
        return wynik;
    }
    public static void insertElement(IntegerSet tab,int x){
        tab.tab[x]=true;
    }
    public static void deleteElement(IntegerSet tab,int x){
        tab.tab[x]=false;
    }
    public static String toString(IntegerSet tab){
        StringBuffer s=new StringBuffer();
        for (int i=1;i<tab.tab.length;i++){
            if(tab.tab[i]) {
                s.append(i);
                s.append(" ");
            }
        }
        return s.toString();
    }
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IntegerSet that = (IntegerSet) o;
        return Arrays.equals(tab, that.tab);
    }
}
