package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Wyklad04;

public class Zadanie2 {
    public static void main(String[] args){
        IntegerSet tab1=new IntegerSet();
        IntegerSet tab2=new IntegerSet();
        IntegerSet.insertElement(tab1,1);
        IntegerSet.insertElement(tab1,2);
        IntegerSet.insertElement(tab1,3);
        IntegerSet.deleteElement(tab1,3);
        IntegerSet.insertElement(tab2,1);
        IntegerSet.insertElement(tab2,2);
        for(int i=0;i<100;i++){
            System.out.print(i);
            System.out.println(IntegerSet.union(tab1,tab2).tab[i]);
        }
        System.out.print(IntegerSet.toString(tab1));
        System.out.print(tab1.equals(tab2));

    }
}
