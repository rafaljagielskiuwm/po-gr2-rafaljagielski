package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Lista12;
import pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Lista12.Kolekcje;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Stack;

public class KolekcjeTest {
    public static void main(String[] args){
        LinkedList pracownicy=new LinkedList();
        pracownicy.add("Kowalski");
        pracownicy.add("Nowak");
        pracownicy.add("Pietruszka");
        pracownicy.add("Marchewka");
        pracownicy.add("Zablocki");
        pracownicy.add("Zielonka");
        pracownicy.add("Jagielski");
        pracownicy.add("Przepiorkowski");
        pracownicy.add("Zawal");
        pracownicy.add("Dziedzic");
        pracownicy.add("Mlynarz");
        pracownicy.add("Strzelec");
        pracownicy.add("Karp");
        System.out.println(Arrays.toString(pracownicy.toArray()));
        Kolekcje.odwroc(pracownicy);
        System.out.println(Arrays.toString(pracownicy.toArray()));
        Kolekcje.redukuj(pracownicy,3);
        System.out.println(Arrays.toString(pracownicy.toArray()));
        System.out.println(Arrays.toString(Kolekcje.div("Lubie placki z dzemem.")));
        System.out.println(Kolekcje.deleteDot("placek."));
        System.out.println(Kolekcje.revert("Ala ma kota. Jej kot lubi myszy."));
        System.out.println(Kolekcje.divideToDigits(2015));
        //Test iterable
        Stack st=new Stack();
        for(int i=0;i<20;i++) st.push(i);
        Kolekcje.print(st);
        LinkedList list=new LinkedList();
        for(int i=0;i<20;i++) list.add(i);
        Kolekcje.print(list);
    }
}
