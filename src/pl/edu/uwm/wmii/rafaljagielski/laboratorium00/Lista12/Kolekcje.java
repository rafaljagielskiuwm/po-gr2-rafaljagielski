package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Lista12;

import java.util.*;

public class Kolekcje<T>{
    public static void redukuj(LinkedList<String> pracownicy,int n){
        for(int i=(n-1);i<pracownicy.size();i+=(n-1)){
          pracownicy.remove(i);
        }
    }
    public static <T> void swap(LinkedList<T> lista, int a, int b){
        T temp=lista.get(a);
        lista.set(a,lista.get(b));
        lista.set(b,temp);
    }
    public static <T> void odwroc(LinkedList<T> lista){
        for(int i=0,j=lista.size()-1;i<=j;i++,j--){
            swap(lista,i,j);
        }
    }
    //Reverting sentence with Stack
    public static String loweringLetters(String sentence){
        StringBuilder s=new StringBuilder(sentence);
        for(int i=0;i<s.length()-1;i++){
            if(Character.isUpperCase(s.charAt(i))){
                s.setCharAt(i, Character.toLowerCase(s.charAt(i)));
            }
        }
        return s.toString();
    }
    public static String[] div(String sentence){
        String[] words=sentence.split(" ");
        return words;
    }
    public static boolean dotOnEnd(String word){
        if(word.charAt(word.length()-1)=='.') return true;
        else return false;
    }
    public static String deleteDot(String word){
        StringBuilder s=new StringBuilder(word);
        s.deleteCharAt(word.length()-1);
        return s.toString();
    }
    public static String addDot(String word){
        return word+".";
    }
    public static String changeFirstLetter(String word){
        StringBuilder s=new StringBuilder(word);
        s.setCharAt(0, Character.toUpperCase(s.charAt(0)));
        return s.toString();
    }
    public static String[] revertOperation(String sentence){
        int j=0;
        String temp;
        String[] words=div(loweringLetters(sentence));
        String[] changed=new String[words.length];
        Stack st=new Stack();
        for(int i=0;i<words.length;i++){
            if(!dotOnEnd(words[i])){
                st.push(words[i]);
            }
            else{
                st.push(changeFirstLetter(deleteDot(words[i])));
                while(!st.empty()){
                    changed[j]=st.pop().toString();
                    j++;
                    if(st.empty()){
                        temp=changed[j-1];
                        changed[j-1]=addDot(temp);
                    }
                }
            }
        }
        return changed;
    }
    public static String revert(String sentence){
        StringBuilder s=new StringBuilder();
        String[] changed=revertOperation(sentence);
        for(int i=0;i<changed.length;i++){
            s.append(changed[i]);
            s.append(" ");
        }
        return s.toString();
    }
    public static String divideToDigits(int number){
        int rest;
        StringBuilder s=new StringBuilder();
        Stack st=new Stack();
        while(number>0) {
            rest = number % 10;
            number /= 10;
            st.push(rest);
        }
        while(!st.empty()){
            s.append(st.pop());
            s.append(" ");
        }
        return s.toString();
    }
    //Sito Eratostenesa
    public static void primes() {
        ArrayList primes = new ArrayList();
        int n;
        Scanner odczyt = new Scanner(System.in);
        System.out.println("Podaj n:");
        n = odczyt.nextInt();
        int[] numbers = new int[n - 2];
        for (int i = 0; i < n-2; i++) {
            numbers[i] = i+2;
        }
        for (int i = 0; i < n - 2; i++) {
            if (numbers[i] != 0) {
                for (int j = 0; j < n - 2; j++) {
                    if ((i != j) && (numbers[j] % numbers[i] == 0)) numbers[j] = 0;
                }
            }
        }
        for (int i = 0; i < n - 2; i++) {
            if (numbers[i] != 0) System.out.print(numbers[i]+", ");
        }
    }
    // print implementing Iterable<E>
    public static void print(List<? extends Iterable> something ){
        for (Object t : something)
            System.out.print(t+", ");
    }
}
