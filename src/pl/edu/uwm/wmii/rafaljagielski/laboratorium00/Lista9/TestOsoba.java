package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Lista9;
import pl.imiajd.jagielski.Osoba3;

import java.time.LocalDate;
import java.util.Arrays;

public class TestOsoba {
    public static void main(String[] args){
        Osoba3 []grupa=new Osoba3[5];
        grupa[0]=new Osoba3("Jagielski", LocalDate.of(1993,8,1));
        grupa[1]=new Osoba3("Kowalski", LocalDate.of(1915,4,3));
        grupa[2]=new Osoba3("Nowak", LocalDate.of(1992,7,1));
        grupa[3]=new Osoba3("Kowalski", LocalDate.of(1986,11,2));
        grupa[4]=new Osoba3("Piekarz", LocalDate.of(1992,7,1));
        for (Osoba3 os : grupa){
            System.out.println(os.toString());
        }
        System.out.println("----------------------------\nPo sortowaniu:\n");
        Arrays.sort(grupa);
        for (Osoba3 os : grupa){
            System.out.println(os.toString()+os.printLifeDuration());
        }
    }
}
