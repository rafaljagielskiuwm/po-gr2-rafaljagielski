package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Wyklad01;
import java.util.Scanner;
import java.lang.Math;
public class Zadanie2 {
    public static void main(String[] args){
        int n;
        int a=0;
        int b=0;
        int c=0;
        int d=0;
        int e=0;
        int f=0;
        int g=0;
        int h=0;
        int silnia=1;
        Scanner odczyt=new Scanner(System.in);
        System.out.println("Podaj n:");
        n=odczyt.nextInt();
        int[] tab=new int[n];
        for (int i=0;i<n;i++) {
            System.out.println("Podaj "+(i+1)+" liczbe: ");
            tab[i]=odczyt.nextInt();
        }
        for (int i=0;i<n;i++){
            if(tab[i]%2!=0){
                a++;
                if(tab[i]>=0) g++;
            }
            else if(i%2==0) f++;
            if(tab[i]%3==0&&tab[i]%5!=0) b++;
            if(Math.sqrt(tab[i])%2==0) c++;
            if((i!=0)&&(i!=(n-1))) {
                if (tab[i] < ((tab[i - 1] + tab[i + 1]) / 2)) d++;
            }
            silnia*=(i+1);
            if((Math.pow(2,(i+1))<tab[i])&&(tab[i]<silnia)) e++;
            if(Math.abs(tab[i])<Math.pow((i+1),2)) h++;
        }
        System.out.println("a="+a+"\n"+
                "b="+b+"\n"+
                "c="+c+"\n"+
                "d="+d+"\n"+
                "e="+e+"\n"+
                "f="+f+"\n"+
                "g="+g+"\n"+
                "h="+h+"\n");
    }
}
