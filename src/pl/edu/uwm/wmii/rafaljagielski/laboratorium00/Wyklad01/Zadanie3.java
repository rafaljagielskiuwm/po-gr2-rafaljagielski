package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Wyklad01;
import java.util.Scanner;
public class Zadanie3 {
    public static void main(String[] args){
        int n;
        int suma=0;
        int dodatnie=0;
        int ujemne=0;
        int zera=0;
        int min=0;
        int max=0;
        int liczbaPar=0;
        Scanner odczyt=new Scanner(System.in);
        System.out.println("Podaj ile liczb naturalnych chcesz wprowadzic: ");
        n=odczyt.nextInt();
        int[] tab=new int[n];
        for (int i=0; i<n;i++) {
            System.out.println("Podaj " + (i + 1) + " liczbe: ");
            tab[i] = odczyt.nextInt();
        }
        min=tab[0];
        max=tab[0];
        for (int i=0; i<n;i++){
            if(tab[i]>0){
                suma+=tab[i]*2;
                dodatnie++;
            }
            else if(tab[i]<0) ujemne++;
            else zera++;
            if(tab[i]<min) min=tab[i];
            if(tab[i]>max) max=tab[i];
        }
        for(int i=1;i<n;i+=2){
            if(i!=(n-1)) {
                if (tab[i] > 0) {
                    if (tab[i - 1] > 0) liczbaPar++;
                    if (tab[i + 1] > 0) liczbaPar++;
                }
            }
            else if(tab[i]>0){
                if(tab[i-1]>0) liczbaPar++;
            }
        }
        System.out.println("Podwojona suma dodanich="+suma+"\n"+
                "dodatnich liczb jest "+dodatnie+", ujemnych jest "+ujemne+", zer jest "+zera+"\n"+
                "najmniejsza wpsiana liczba to "+min+"\n"+
                "najwieksza wpisana liczba to "+max+"\n"+
                "liczba par dodatnich wynosi: "+liczbaPar+"\n");



    }
}
