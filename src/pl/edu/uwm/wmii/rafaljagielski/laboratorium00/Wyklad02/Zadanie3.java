package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Wyklad02;
import java.util.Scanner;
import java.util.Random;
public class Zadanie3 {
    public static void main(String[] args){
        int[] tab=new int[3];
        int m,n,k;
        int[][] a,b,c;
        wczytanie(tab);
        m=tab[0];
        n=tab[1];
        k=tab[2];
        a=new int[m][n];
        b=new int[n][k];
        c=new int[m][k];
        generuj(a,0,9);
        generuj(b,0,9);
        System.out.println("Macierz a: ");
        wyswietl(a);
        System.out.println("Macierz b: ");
        wyswietl(b);
        iloczyn(a,b,c);
        System.out.println("Macierz c: ");
        wyswietl(c);
    }
    public static void wczytanie(int[] tab){
        Scanner odczyt=new Scanner(System.in);
        while(!(tab[0]>=1&&tab[0]<=10)){
            System.out.print("Podaj m: ");
            tab[0]=odczyt.nextInt();
            System.out.println();
            if(!(tab[0]>=1&&tab[0]<=10)) System.out.println("Podano wartosc zpoza zakresu!");
        }
        while(!(tab[1]>=1&&tab[1]<=10)){
            System.out.print("Podaj n: ");
            tab[1]=odczyt.nextInt();
            System.out.println();
            if(!(tab[1]>=1&&tab[1]<=10)) System.out.println("Podano wartosc zpoza zakresu!");
        }
        while(!(tab[2]>=1&&tab[2]<=10)){
            System.out.print("Podaj k: ");
            tab[2]=odczyt.nextInt();
            System.out.println();
            if(!(tab[2]>=1&&tab[2]<=10)) System.out.println("Podano wartosc zpoza zakresu!");
        }
    }
    public static void generuj(int[] tab,int zakresMin, int zakresMax){
        Random los=new Random();
        for (int i=0;i<tab.length;i++){
            tab[i]=los.nextInt(zakresMax+zakresMin+1)-zakresMin;
        }
    }
    public static void generuj(int[][] tab,int zakresMin,int zakresMax){
        for (int i=0;i<tab.length;i++){
            generuj(tab[i],zakresMin,zakresMax);
        }
    }
    public static void wyswietl(int[] tab){
        for (int i=0;i<tab.length;i++){
            System.out.print(tab[i]+" ");
        }
        System.out.println();
    }
    public static void wyswietl(int[][] tab){
        for (int i=0;i<tab.length;i++) {
            wyswietl(tab[i]);
        }
    }
    public static void iloczyn(int[][] a,int[][] b, int[][] c){
        for (int k=0;k<b[0].length;k++){
            for(int j=0;j<a.length;j++){
                c[j][k]=0;
                for(int i=0;i<b.length;i++){
                    c[j][k]+=a[j][i]*b[i][k];
                }
            }
        }
    }
}
