package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Wyklad02;
import java.util.Scanner;
import java.util.Random;

public class Zadanie1 {
    public static void main(String[] args){
        int n=0;
        int parzyste=0;
        int nieparzyste=0;
        int ujemne=0;
        int dodatnie=0;
        int zerowe=0;
        int max=0;
        int ileRazy=0;
        int sumaUjemnch=0;
        int sumaDodatnich=0;
        int dlugoscFragmentuDodatniego=0;
        int sprawdzenieDlugosciFragmetu=0;
        int lewy=0;
        int prawy=0;
        int pom=0;
        Scanner odczyt=new Scanner(System.in);
        //Wprowadzanie n
        while(!(1<=n&&n<=100)) {
            System.out.println("Podaj n:");
            n = odczyt.nextInt();
            if (!(1 <= n && n <= 100)) {
                System.out.println("Podano n z poza zakresu");
            }
        }
        //Generowanie tablicy, ilosc i suma ujemnych dodatnich zerowych
        int[] tab=new int[n];
        Random generator=new Random();
        for(int i=0;i<n;i++) {
            tab[i] = generator.nextInt(1998)-999;
            if(tab[i]%2==0) parzyste++;
            else nieparzyste++;
            if(tab[i]<0){
                ujemne++;
                sumaUjemnch+=tab[i];
            }
            if(tab[i]>0) {
                dodatnie++;
                sumaDodatnich+=tab[i];
            }
            if(tab[i]==0) zerowe++;
        }
        //Wyswietlenie tablicy
        System.out.println("Wyswietlamy wygenreowana tablice");
        for (int i=0;i<n;i++){
            System.out.print(tab[i]+", ");
        }
        System.out.println();
        //Maksimum w tablicy
        max=tab[0];
        for (int i=0;i<n;i++){
            if(tab[i]>max) max=tab[i];
        }
        //Ile razy wystapilo maksimum
        for (int i=0;i<n;i++){
            if(tab[i]==max) ileRazy++;
        }
        //Dlugosc najdluzszego fragmentu zmiennych dodatnich w tablicy
        for (int i=0;i<n;i++){
            if(tab[i]>0) sprawdzenieDlugosciFragmetu++;
            else{
                if(dlugoscFragmentuDodatniego < sprawdzenieDlugosciFragmetu) {
                    dlugoscFragmentuDodatniego=sprawdzenieDlugosciFragmetu;
                }
                sprawdzenieDlugosciFragmetu=0;
            }
        }
        //Odwrocenie fragmentu tablicy podawanie zakresu
        System.out.println("Podaj dwie zmienne wyznaczajace fragment tablicy ktory ma zosrac odwrocony: ");
        while(!(1<=lewy&&lewy<=100)) {
            System.out.println("Podaj lewy kraniec:");
            lewy = odczyt.nextInt();
            if (!(1 <= lewy && lewy <= 100)) {
                System.out.println("Podano zmienna z poza zakresu");
            }
        }
        while(!(1<=prawy&&prawy<=100&&prawy>lewy)) {
            System.out.println("Podaj prawy kraniec:");
            prawy = odczyt.nextInt();
            if (!(1 <= prawy && prawy <= 100)) {
                System.out.println("Podano zmienna z poza zakresu");
            }
            if(prawy<=lewy) System.out.println("Zakres prawy nie moze byc mniejszy lub rowny lewemu");
        }
        //Owrocenie fragmentu wlasciwe
        int j=prawy;
        for (int i=lewy;i<prawy;i++){
            if(j>i) {
                pom = tab[i];
                tab[i] = tab[j];
                tab[j] = pom;
            }
            j--;
            }
        System.out.println("Wyswietlamy tablice po odwroceniu fragmentu");
        for (int i=0;i<n;i++){
            System.out.print(tab[i]+", ");
        }
        System.out.println();
        //Zamiana dodatnich zmiennych na 1 ujemnych na -1
        for (int i=0;i<n;i++){
            if (tab[i]>=0) tab[i]=1;
            else tab[i]=-1;
        }
        //Wyswietlenie powyzszej zmiany
        System.out.println("Wyswietlamy tablice gdzie dodatnie wartosci zostaly zamienione na 1 a ujemne na -1");
        for(int i=0;i<n;i++){
            System.out.print(tab[i]+", ");
        }
        System.out.println();
        //Wyswietlenie wynikow
        System.out.println("Ilosc nieparzystych: "+nieparzyste+"\n"+
                "Ilosc parzystych: "+parzyste+"\n"+
                "Ilosc ujemnych: "+ujemne+"\n"+
                "Ilosc dodatnich: "+dodatnie+"\n"+
                "Ilosc zerowch: "+zerowe+"\n"+
                "Maksimum w tablicy: "+max+"\n"+
                "Ile razy wystapilo max: "+ileRazy+"\n"+
                "Suma elementow dodatnich: "+sumaDodatnich+"\n"+
                "Suma elementow ujemnych: "+sumaUjemnch+"\n"+
                "Dlugosc najdluzszego fragmentu zmiennych dodatnich: "+dlugoscFragmentuDodatniego+"\n");



    }
}
