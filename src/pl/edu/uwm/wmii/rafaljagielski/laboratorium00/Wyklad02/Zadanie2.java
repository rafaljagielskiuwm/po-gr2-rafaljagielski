package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Wyklad02;
import java.util.Random;
public class Zadanie2 {
    public static void main(String[] args){

    }
    public static void generuj(int tab[],int n,int minWartowsc, int maxWartosc){
        Random generator=new Random();
        for (int i=0;i<n;i++){
        tab[i]=generator.nextInt(maxWartosc+minWartowsc)-minWartowsc;
        }
    }
    public static int ileNieparzystych(int tab[]){
        int nieparzyste=0;
        for(int i=0;i<tab.length;i++) if(tab[i]%2!=0) nieparzyste++;
        return nieparzyste;
    }
    public static int ileParzystych(int tab[]){
        int parzyste=0;
        for (int i=0;i<tab.length;i++) if(tab[i]%2==0) parzyste++;
        return parzyste;
    }
    public static int ileDodatnich(int tab[]){
        int dodatnie=0;
        for (int i=0;i<tab.length;i++) if(tab[i]>0) dodatnie++;
        return dodatnie;
    }
    public static int ileUjemnych(int tab[]){
        int ujemne=0;
        for (int i=0;i<tab.length;i++) if(tab[i]<0) ujemne++;
        return ujemne;
    }
    public static int ileZerowych(int tab[]){
        int zerowe=0;
        for (int i=0; i<tab.length;i++) if(tab[i]==0) zerowe++;
        return zerowe;
    }
    public static int ileMaksymalnych(int tab[]){
        //Maksimum w tablicy
        int max=tab[0];
        int ileRazy=0;
        for (int i=0;i<tab.length;i++){
            if(tab[i]>max) max=tab[i];
        }
        //Ile razy wystapilo maksimum
        for (int i=0;i<tab.length;i++){
            if(tab[i]==max) ileRazy++;
        }
        return ileRazy;
    }
    public static int sumaDodatnich(int tab[]){
        int suma=0;
        for (int i=0;i<tab.length;i++) if(tab[i]>=0) suma+=tab[i];
        return suma;
    }
    public static int sumaUjemnych(int tab[]){
        int suma=0;
        for (int i=0;i<tab.length;i++) if(tab[i]<0) suma+=tab[i];
        return suma;
    }
    public static int dlugoscMaksymalnegoCiaguDodatnich(int tab[]){
        int sprawdzenieDlugosciFragmetu=0;
        int dlugoscFragmentuDodatniego=0;
        for (int i=0;i<tab.length;i++){
            if(tab[i]>0) sprawdzenieDlugosciFragmetu++;
            else{
                if(dlugoscFragmentuDodatniego < sprawdzenieDlugosciFragmetu) {
                    dlugoscFragmentuDodatniego=sprawdzenieDlugosciFragmetu;
                }
                sprawdzenieDlugosciFragmetu=0;
            }
        }
        return dlugoscFragmentuDodatniego;
    }
    public static void signum(int tab[]){
        for (int i=0;i<tab.length;i++){
            if (tab[i]>=0) tab[i]=1;
            else tab[i]=-1;
        }
    }
    public static void odwrocFragment(int tab[],int lewy, int prawy){
        int j=prawy;
        int pom=0;
        for (int i=lewy;i<=prawy;i++){
            if(j>i){
                pom=tab[i];
                tab[i]=tab[j];
                tab[j]=pom;
            }
            j--;
        }
    }
}
