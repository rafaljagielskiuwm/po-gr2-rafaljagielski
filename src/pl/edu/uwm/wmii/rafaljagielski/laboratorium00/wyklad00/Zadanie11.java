package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.wyklad00;

public class Zadanie11 {
    public static void main(String[] args) {
        System.out.println("Bajeczka o osiołku\n" +
                "\n" +
                "Osiołkowi w żłoby dano,\n" +
                "W jeden owies, w drugi, siano.\n" +
                "Uchem strzyże, głową kręci.\n" +
                "I to pachnie, i to nęci.\n" +
                "Od któregoż teraz zacznie,\n" +
                "Aby sobie podjeść smacznie?\n" +
                "Trudny wybór, trudna zgoda -\n" +
                "Chwyci siano, owsa szkoda.\n" +
                "Chwyci owies, żal mu siana.\n" +
                "I tak stoi aż do rana,\n" +
                "A od rana do wieczora;\n" +
                "Aż nareszcie przyszła pora,\n" +
                "Ze oślina pośród jadła -\n" +
                "Z głodu padła.\n");
    }
}
