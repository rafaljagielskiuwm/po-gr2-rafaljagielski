package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Lista10;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;

public class OdczytZPliku {
    public static void main(String[] args){
        ArrayList<String> lines = new ArrayList<>();
        BufferedReader reader;
        try{
         reader= new BufferedReader(new FileReader("D:\\Bitbucket\\java\\po-gr2-rafaljagielski\\src\\pl\\edu\\uwm\\wmii\\rafaljagielski\\laboratorium00\\Lista10\\test.txt"));
        String line=reader.readLine();
        while (line != null){
                lines.add(line);
                line=reader.readLine();
            }
            reader.close();
        }catch (IOException e){
            e.printStackTrace();
        }
        System.out.println(lines.toString());
        Collections.sort(lines);
        System.out.println(lines.toString());
    }
}
