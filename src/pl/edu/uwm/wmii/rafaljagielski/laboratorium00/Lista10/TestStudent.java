package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Lista10;
import pl.imiajd.jagielski.Student3;

import java.time.LocalDate;
import java.util.Arrays;

public class TestStudent {
    public static void main(String[] args){
        Student3 []grupa=new Student3[5];
        grupa[0]=new Student3("Jagielski", LocalDate.of(1993,8,1),3.14);
        grupa[1]=new Student3("Kowalski", LocalDate.of(1915,4,3),4.50);
        grupa[2]=new Student3("Nowak", LocalDate.of(1992,7,1),4.12);
        grupa[3]=new Student3("Kowalski", LocalDate.of(1986,11,2),5.00);
        grupa[4]=new Student3("Piekarz", LocalDate.of(1992,7,1),4.89);
        for (Student3 os : grupa){
            System.out.println(os.toString());
        }
        System.out.println("----------------------------\nPo sortowaniu:\n");
        Arrays.sort(grupa);
        for (Student3 os : grupa){
            System.out.println(os.toString()+os.printLifeDuration());
        }
    }
}
