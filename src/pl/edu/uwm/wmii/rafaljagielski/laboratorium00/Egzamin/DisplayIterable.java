package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Egzamin;

import java.util.List;

public class DisplayIterable {
    public static void print(Iterable<?> something){
        for(Object o : something ){
            System.out.println(o);
        }
    }
}
