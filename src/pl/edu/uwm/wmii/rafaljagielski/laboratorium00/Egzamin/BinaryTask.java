package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Egzamin;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class BinaryTask {
    public static void main(String[] args) throws IOException {
        BinaryTask binary = new BinaryTask();
        Path path = Paths.get("C:\\Users\\Mistrz\\Desktop\\Student.class");
        byte[] bytes = Files.readAllBytes(path);
//        for( int i=0; i<bytes.length;i++){
//            System.out.println(bytes[i]);
//        }
        for(int i=0;i<bytes.length;i++){
            if((bytes[i]<=126)&&(bytes[i]>=32)){
                System.out.print((char)bytes[i]);
            }
            if(i%63==0){
                System.out.println();
            }
        }
    }
}
