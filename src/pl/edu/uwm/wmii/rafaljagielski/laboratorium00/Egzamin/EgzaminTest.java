package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Egzamin;

import java.util.ArrayList;
import java.time.LocalDate;
public class EgzaminTest {
    public static void main(String[] args){
        ArrayList<Integer> tab=new ArrayList<>();
        tab.add(1);
        tab.add(2);
        tab.add(1);
        tab.add(4);
        tab.add(5);
        System.out.println(Decreasing.isSorted(tab));

        ArrayList<LocalDate> tab2=new ArrayList<>();
        tab2.add(LocalDate.of(1993,12,1));
        tab2.add(LocalDate.of(1994,12,1));
        tab2.add(LocalDate.of(1992,12,1));
        tab2.add(LocalDate.of(1996,12,1));
        tab2.add(LocalDate.of(1997,12,1));
        System.out.println(Decreasing.isSorted(tab2));

        DisplayIterable.print(tab2);

    }

}
