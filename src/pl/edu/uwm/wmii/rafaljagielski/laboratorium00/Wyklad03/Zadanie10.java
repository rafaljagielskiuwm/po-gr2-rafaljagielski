package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Wyklad03;
import java.math.BigInteger;
public class Zadanie10 {
    public static void main(String[] args){
        int n=3;
        System.out.println(szachy(n));
    }
    public static BigInteger szachy(int n){
        BigInteger ziarna=BigInteger.ZERO;
        for (int i=0;i<n;i++){
            ziarna=ziarna.add(BigInteger.valueOf(2).pow(i));
        }
        return ziarna;
    }
}
