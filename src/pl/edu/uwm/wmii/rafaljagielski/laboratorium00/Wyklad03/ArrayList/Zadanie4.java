package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Wyklad03.ArrayList;
import java.awt.image.AreaAveragingScaleFilter;
import java.util.ArrayList;
public class Zadanie4 {
    public static void main(String[] args){
        ArrayList<Integer>a=new ArrayList<Integer>();
        ArrayList<Integer>odwrocona=new ArrayList<Integer>();
        a.add(1);
        a.add(2);
        a.add(3);
        a.add(4);
        a.add(5);
        odwrocona=reversed(a);
        for (int i=0;i<odwrocona.size();i++){
            System.out.print(odwrocona.get(i));
        }

    }
    public static ArrayList<Integer> reversed(ArrayList<Integer>a){
        ArrayList<Integer>odwrocona=new ArrayList<Integer>();
        for (int i=a.size()-1;i>=0;i--){
            odwrocona.add(a.get(i));
        }
        return odwrocona;
    }
}
