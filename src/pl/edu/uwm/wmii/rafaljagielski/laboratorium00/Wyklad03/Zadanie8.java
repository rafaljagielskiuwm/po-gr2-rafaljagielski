package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Wyklad03;
import java.util.Scanner;
public class Zadanie8 {
    public static void main(String[] args){
        String str;
        char seperator;
        int pozycja;
        String wynik;
        Scanner odczyt=new Scanner(System.in);
        System.out.println("Podaj liczbe: ");
        str=odczyt.nextLine();
        System.out.println("Podaj separator: ");
        seperator=odczyt.next().charAt(0);
        System.out.println("Podaj odstep: ");
        pozycja=odczyt.nextInt();
        wynik=nice(str,seperator,pozycja);
        System.out.println(wynik);
    }
    public static String nice(String str,char separator,int pozycja){
        int j=str.length()%pozycja;
        if(j==0) j=pozycja;
        StringBuffer sb=new StringBuffer();
        for(int i=0;i<str.length();i++){
            if(i==j){
                sb.append(separator);
                sb.append(str.charAt(i));
                j+=pozycja;
            }
            else{
                sb.append(str.charAt(i));
            }
        }
        return sb.toString();
    }
}
