package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Wyklad03.ArrayList;
import java.util.ArrayList;
public class Zadanie3 {
    public static void main(String[] args){
        ArrayList<Integer>a=new ArrayList<Integer>();
        ArrayList<Integer>b=new ArrayList<Integer>();
        ArrayList<Integer>c=new ArrayList<Integer>();
        a.add(1);
        a.add(2);
        b.add(2);
        b.add(3);
        b.add(3);
        b.add(4);
        b.add(5);
        b.add(10);
        c=mergeSorted(a,b);
        for (int i=0;i<c.size();i++){
            System.out.print(c.get(i));
        }
    }
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer>a,ArrayList<Integer>b){
        ArrayList<Integer>c=new ArrayList<Integer>();;
        int start=0;
        for(int i=0;i<a.size();i++){
            for (int j=start;j<b.size();j++){
                if(a.get(i)>=b.get(j)){
                    c.add(b.get(j));
                    start++;
                }
            }
            c.add(a.get(i));
        }
        for (;start<b.size();start++){
            c.add(b.get(start));
        }
        return c;
    }
}
