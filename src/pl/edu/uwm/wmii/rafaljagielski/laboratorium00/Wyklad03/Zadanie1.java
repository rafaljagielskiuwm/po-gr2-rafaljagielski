package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Wyklad03;
import java.util.Scanner;
public class Zadanie1 {
    public static void main(String[] args){
        String napis;
        char znak;
        Scanner odczyt=new Scanner(System.in);
        System.out.println("Podaj napis: ");
        napis=odczyt.nextLine();
        System.out.println("Podaj znak ktorego ilosc wystapien w napisie chcesz zliczyc: ");
        znak=odczyt.next().charAt(0);
        System.out.println(znak+" wystapil "+countChar(napis,znak)+" razy w napisie: "+napis);

    }
    public static int countChar(String str, char c){
        int licz=0;
        for (int i=0;i<str.length();i++){
            if(str.charAt(i)==c) licz++;
        }
        return licz;
    }
}
