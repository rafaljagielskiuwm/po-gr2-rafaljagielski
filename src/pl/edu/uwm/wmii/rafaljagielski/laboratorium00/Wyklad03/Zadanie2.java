package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Wyklad03;
import java.util.Scanner;
public class Zadanie2 {
    public static void main(String[] args){
        String napis1;
        String napis2;
        Scanner odczyt=new Scanner(System.in);
        System.out.println("Podaj pierwszy napis");
        napis1=odczyt.nextLine();
        System.out.println("Podaj drugi napis");
        napis2=odczyt.nextLine();
        System.out.println("Drugi napis zawiera sie "+countSubStr(napis1,napis2)+" razy w napisie pierwszym");
    }
    public static int countSubStr(String str, String subStr){
        int licz=0;
        for (int i=0;i<str.length()-subStr.length()+1;i++){
            if(str.substring(i,(subStr.length()+i)).equals(subStr)) licz++;
        }
        return licz;
    }
}
