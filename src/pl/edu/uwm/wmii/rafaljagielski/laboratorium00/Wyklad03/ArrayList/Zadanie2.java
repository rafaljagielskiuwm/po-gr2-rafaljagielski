package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Wyklad03.ArrayList;
import java.util.ArrayList;
public class Zadanie2 {
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<Integer>();
        ArrayList<Integer> b = new ArrayList<Integer>();
        a.add(1);
        a.add(2);
        b.add(3);
        b.add(4);
        b.add(5);
        b.add(6);
        ArrayList<Integer> c = merge(a, b);
        for (int i = 0; i < c.size(); i++) {
            System.out.print(c.get(i));
        }
    }

    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> c = new ArrayList<>();
        int size;
        if (a.size() > b.size()) {
            size = a.size();
            for (int i = 0; i < size; i++) {
                if (i < b.size()) {
                    c.add(a.get(i));
                    c.add(b.get(i));
                } else
                    c.add(a.get(i));
            }
        } else {
            size = b.size();
            for (int i = 0; i < size; i++) {
                if (i < a.size()) {
                    c.add(a.get(i));
                    c.add(b.get(i));
                } else
                    c.add(b.get(i));
            }
        }
        return c;
    }
}