package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Wyklad03;
import java.util.Scanner;
public class Zadanie3 {
    public static void main(String[] args){
        String napis;
        Scanner odczyt=new Scanner(System.in);
        System.out.println("Podaj napis: ");
        napis=odczyt.nextLine();
        System.out.println("Wynik metody: "+middle(napis));
    }
    public static String middle(String str){
        if(str.length()%2!=0) return String.valueOf(str.charAt(str.length()/2));
        else return str.substring(((str.length()/2)-1),((str.length()/2)+1));
    }
}
