package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Wyklad03;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Zadanie9 {
    public static void main(String[] args)throws Exception{
        String filePath="C:\\Bitbucket\\po-gr2-rafaljagielski\\src\\pl\\edu\\uwm\\wmii\\rafaljagielski\\laboratorium00\\Wyklad03\\file.txt";
        System.out.println(fileToString(filePath));
        System.out.println(countChar(fileToString(filePath),'a'));
        System.out.println(countSubStr(fileToString(filePath),"ala"));
    }
    public static String fileToString(String filePath) throws Exception{
        BufferedReader fileReader=null;
        try{
            fileReader=new BufferedReader(new FileReader(filePath));
            String odczyt=fileReader.readLine();
            return odczyt;
        }
        finally{
            if(fileReader !=null){
                fileReader.close();
            }
        }
    }
    public static int countChar(String zawartoscPliku,char c){
        int licz=0;
        for (int i=0;i<zawartoscPliku.length();i++){
            if(zawartoscPliku.charAt(i)==c){
                licz++;
            }
        }
        return licz;
    }
    public static int countSubStr(String zawartoscPliku, String subStr){
        int licz=0;
        for (int i=0;i<zawartoscPliku.length()-subStr.length()+1;i++){
            if((zawartoscPliku.substring(i,i+subStr.length())).equals(subStr)){
                licz++;
            }
        }
        return licz;
    }
}
