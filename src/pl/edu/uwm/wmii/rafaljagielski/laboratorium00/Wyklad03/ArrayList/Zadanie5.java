package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Wyklad03.ArrayList;
import java.util.ArrayList;
public class Zadanie5 {
    public static void main(String[] args){
        ArrayList<Integer>a=new ArrayList<Integer>();
        a.add(1);
        a.add(2);
        a.add(3);
        a.add(4);
        System.out.println("ArrayList przed odworceniem:");
        for (int i=0;i<a.size();i++){
            System.out.print(a.get(i));
        }
        reverse(a);
        System.out.println("\nArrayList po odworceniu:");
        for (int i=0;i<a.size();i++){
            System.out.print(a.get(i));
        }
    }
    public static void reverse(ArrayList<Integer>a){
        ArrayList<Integer>odwrocona = new ArrayList<Integer>();
        for (int i=a.size()-1;i>=0;i--){
            odwrocona.add(a.get(i));
        }
        a.clear();
        for (int i=0;i<odwrocona.size();i++){
            a.add(odwrocona.get(i));
        }
    }
}
