package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Wyklad03;
import java.util.Scanner;
public class Zadanie7 {
    public static void main(String[] args){
        String str;
        String wynik;
        Scanner odczyt=new Scanner(System.in);
        System.out.println("Podaj liczbe: ");
        str=odczyt.nextLine();
        wynik=nice(str);
        System.out.println(wynik);
    }
    public static String nice(String str){
        int j=str.length()%3;
        if(j==0) j=3;
        StringBuffer sb=new StringBuffer();
        for(int i=0;i<str.length();i++){
            if(i==j){
                sb.append("'");
                sb.append(str.charAt(i));
                j+=3;
            }
            else{
                sb.append(str.charAt(i));
            }
        }
        return sb.toString();
    }
}
