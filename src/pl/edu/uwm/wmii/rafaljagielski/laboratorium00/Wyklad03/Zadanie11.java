package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Wyklad03;
import java.math.BigDecimal;
public class Zadanie11 {
    public static void main(String[] args){
        System.out.println(odsetki(1000,10,2));
    }
    public static BigDecimal odsetki(int k,int p, int n){
        BigDecimal wynik=BigDecimal.ZERO;
        wynik=wynik.valueOf(k);
        BigDecimal procent=BigDecimal.valueOf(p).divide(BigDecimal.valueOf(100));
        for (int i=0;i<n;i++){
            wynik=wynik.add(wynik.multiply(procent));
        }
        return wynik;
    }
}
