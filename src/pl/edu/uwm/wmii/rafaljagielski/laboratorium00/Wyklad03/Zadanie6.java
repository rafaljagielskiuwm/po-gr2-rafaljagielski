package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Wyklad03;
import java.util.Scanner;
public class Zadanie6 {
    public static void main(String[] args){
        String str;
        Scanner odczyt=new Scanner(System.in);
        System.out.println("Podaj napis: ");
        str=odczyt.nextLine();
        System.out.println("Napis po zmianie to: "+change(str));

    }
    public static String change(String str){
        StringBuffer sb=new StringBuffer();
        for (int i=0;i<str.length();i++){
            if(Character.isLowerCase(str.charAt(i))){
                sb.append(Character.toUpperCase(str.charAt(i)));
            }
            else {
                sb.append(Character.toLowerCase(str.charAt(i)));
            }
        }
        return sb.toString();
    }
}



