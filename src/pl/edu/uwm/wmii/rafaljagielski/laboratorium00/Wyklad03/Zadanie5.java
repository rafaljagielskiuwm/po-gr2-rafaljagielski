package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Wyklad03;
import java.util.Scanner;
public class Zadanie5 {
    public static void main(String[] args){
        String napis1;
        String napis2;
        Scanner odczyt=new Scanner(System.in);
        System.out.println("Podaj pierwszy napis: ");
        napis1=odczyt.nextLine();
        System.out.println("Podaj drugi napis: ");
        napis2=odczyt.nextLine();
        int []tab=where(napis1,napis2);
        for (int i=0;i<tab.length;i++){
            System.out.println(tab[i]);
        }

    }
    public static int[] where(String str, String subStr){
        int licz=0;
        int index=0;
        int[] tab;
        for (int i=0;i<str.length()-subStr.length()+1;i++){
            if(str.substring(i,(subStr.length()+i)).equals(subStr)) licz++;
        }
        tab=new int[licz];
        for (int i=0;i<str.length()-subStr.length()+1;i++){
            if(str.substring(i,(subStr.length()+i)).equals(subStr)) {
                tab[index]=i;
                index++;
            }
        }
        return tab;
    }
}
