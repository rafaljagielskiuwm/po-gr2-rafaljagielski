package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Lista11;

public class PairUtilDemo {
    public static void main(String[] args){
        Pair<Integer>pair = new Pair<Integer>(1,3);
        System.out.println(String.format("%s %s", PairUtil.swap(pair).getFirst(), PairUtil.swap(pair).getSecond()));
    }
}
