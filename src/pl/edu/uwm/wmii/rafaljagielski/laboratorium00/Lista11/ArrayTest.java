package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Lista11;

import pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Lista12.Kolekcje;

import java.time.LocalDate;
import java.util.ArrayList;

public class ArrayTest {
        public static void main(String[] args) {
            ArrayList<Integer> sorted = new ArrayList<>();
            sorted.add(1);
            sorted.add(2);
            sorted.add(3);
            sorted.add(4);
            sorted.add(5);
            ArrayList<Integer> unSorted = new ArrayList<>();
            unSorted.add(5);
            unSorted.add(2);
            unSorted.add(3);
            unSorted.add(1);
            unSorted.add(4);
            System.out.println(ArrayUtil.isSorted(sorted));
            System.out.println(ArrayUtil.isSorted(unSorted));
            System.out.println(ArrayUtil.binSearch(sorted,3));
            ArrayUtil.mergeSort(unSorted);
            System.out.println(unSorted);
        }
}
