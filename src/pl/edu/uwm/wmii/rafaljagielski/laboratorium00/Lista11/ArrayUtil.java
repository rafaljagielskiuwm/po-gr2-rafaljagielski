package pl.edu.uwm.wmii.rafaljagielski.laboratorium00.Lista11;
import java.util.ArrayList;

public class ArrayUtil<T> {
    public static <T extends Comparable<T>> boolean isSorted(ArrayList<T>array){
        for(int i=0;i<array.size()-1;i++){
            if(array.get(i).compareTo(array.get(i+1))==1){
                return false;
            }
        }
        return true;
    }
    public static <T extends Comparable<T>> int binSearch(ArrayList<T> array, T searched) {
        int l = 0;
        int r = array.size() - 1;
        int pivot;
        while (l <= r) {
            pivot = r / 2;
            if (array.get(pivot).compareTo(searched) < 0)
                l = pivot + 1;
            else if (array.get(pivot).compareTo(searched) > 0)
                r = pivot - 1;
            else return pivot;
        }
        return -1;
    }
    public static <T extends Comparable<T>> int minIndex(ArrayList<T> array, int start){
        T min=array.get(start);
        int index=start;
        for(int i=start+1; i<array.size();i++){
            if(min.compareTo(array.get(i))==1){
                min=array.get(i);
                index=i;
            }
        }
        return index;
    }
    public static <T extends Comparable<T>> void swap(ArrayList<T> array, int a, int b){
        T temp=array.get(a);
        array.set(a,array.get(b));
        array.set(b,temp);
    }
    public static <T extends Comparable<T>> void selectionSort(ArrayList<T> array){
        for(int i=0;i<array.size();i++){
            swap(array,minIndex(array,i),i);
        }
    }
    public static <T extends Comparable<T>> void merge(ArrayList<T> array, int l, int m, int r)
    {
        int n1 = m - l + 1;
        int n2 = r - m;
        ArrayList<T> L = new ArrayList<>();
        for(int i=0;i<n1+100;i++){L.add(array.get(l));}
        ArrayList<T> R = new ArrayList<>();
        for(int i=0;i<n2+100;i++){R.add(array.get(l));}

        for (int i=0; i<n1; ++i)
            L.set(i,array.get(l + i));
        for (int j=0; j<n2; ++j)
            R.set(j,array.get(m +1+ j));

        int i = 0, j = 0;
        int k = l;
        while (i < n1 && j < n2)
        {
            if (!(L.get(i).compareTo(R.get(j))>0))
            {
                array.set(k,L.get(i));
                i++;
            }
            else
            {
                array.set(k,R.get(j));
                j++;
            }
            k++;
        }
        while (i < n1)
        {
            array.set(k,L.get(i));
            i++;
            k++;
        }
        while (j < n2)
        {
            array.set(k,R.get(j));
            j++;
            k++;
        }
    }
    public static <T extends Comparable<T>> void mergeSort(ArrayList<T> array, int l, int r)
    {
        if (l < r)
        {
            int m = (l+r)/2;
            mergeSort(array, l, m);
            mergeSort(array , m+1, r);
            merge(array, l, m, r);
        }
    }
    @SuppressWarnings("unchecked")
    public static <T extends Comparable<T>> void mergeSort(ArrayList<T> array){
        mergeSort(array,0,array.size()-1 );
    }
}
