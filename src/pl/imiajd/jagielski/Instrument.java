package pl.imiajd.jagielski;
import java.time.LocalDate;
import java.util.Objects;

public abstract class Instrument {
    public Instrument(String producent, LocalDate rokProdukcji) {
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
    }
    public String getProducent() {
        return this.producent;
    }
    public LocalDate getRokProdukcji() {
        return this.rokProdukcji;
    }
    public abstract String dzwiek();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Instrument that = (Instrument) o;
        return Objects.equals(producent, that.producent) &&
                Objects.equals(rokProdukcji, that.rokProdukcji);
    }
    @Override
    public String toString() {
        return String.format("Producent: %s , data produkcji: %d - %d - %d ", this.getProducent(), this.getRokProdukcji().getDayOfMonth(), this.getRokProdukcji().getMonthValue(), this.getRokProdukcji().getYear());
    }
    private String producent;
    private LocalDate rokProdukcji;
}
