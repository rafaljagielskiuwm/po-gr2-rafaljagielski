package pl.imiajd.jagielski;
import java.awt.Rectangle;

public class BetterRectangle extends Rectangle {
    public BetterRectangle(int x, int y, int height, int width) { super(x, y, width, height); }
    public double getPerimeter() {
        return 2*getWidth()+2*getHeight();
    }
    public double getArea() {
        return this.getWidth() * this.getHeight();
    }
}