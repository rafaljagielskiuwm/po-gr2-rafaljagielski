package pl.imiajd.jagielski;

public class Adres {
    public Adres(String ulica, int numer_domu, int numer_mieszkania, String miasto, String kod_pocztowy) {
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = numer_mieszkania;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }
    public Adres(String ulica, int numer_domu, String miasto,  String kod_pocztowy) {
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.miasto = miasto;
        this.numer_mieszkania = 0;
        this.kod_pocztowy = kod_pocztowy;
    }
    public void pokaz() {
        System.out.println(String.format("%s\n%d\n%s\n%s\n%s", this.ulica, this.numer_domu, this.numer_mieszkania == 0 ? "/nie podano nr mieszkania": String.format("/%d", this.numer_mieszkania), this.miasto, this.kod_pocztowy));
    }
    public boolean przed(Adres adr) {
        return Integer.parseInt(this.kod_pocztowy) < Integer.parseInt(adr.kod_pocztowy);
    }
    private String ulica;
    private int numer_domu;
    private int numer_mieszkania;
    private String miasto;
    private String kod_pocztowy;
}
