package pl.imiajd.jagielski;

public class Osoba {
    public Osoba(String nazwisko, int rokUrodzenia){
        this.nazwisko=nazwisko;
        this.rokUrodzenia=rokUrodzenia;
    }
    public String getNazwisko(){
        return this.nazwisko;
    }
    public int getRokUrodzenia(){
        return this.rokUrodzenia;
    }
    @Override
    public String toString() {
        return String.format("%s - %d", this.nazwisko, this.rokUrodzenia);
    }
    private String nazwisko;
    private int rokUrodzenia;
}
