package pl.imiajd.jagielski;
import java.time.LocalDate;
import java.util.Objects;
import java.time.Period;

public class Osoba3 implements Comparable<Osoba3> {
    public Osoba3(String nazwisko, LocalDate dataUrodzenia){
        this.nazwisko=nazwisko;
        this.dataUrodzenia=dataUrodzenia;
    }
    public String getNazwisko(){
        return this.nazwisko;
    }
    public LocalDate getDataUrodzenia(){
        return dataUrodzenia;
    }
    public Period summedPeriod(){
        return Period.between(this.dataUrodzenia,LocalDate.now());
    }
    public int ileLat(){
        return summedPeriod().getYears();
    }
    public int ileMiesiecy(){
        return summedPeriod().getMonths();
    }
    public int ileDni(){
        return summedPeriod().getDays();
    }
    public String printLifeDuration(){
        return String.format(" Lat: %d, miesiecy: %d, dni: %d",this.ileLat(),this.ileMiesiecy(),this.ileDni());
    }
    @Override
    public String toString(){
        return String.format("%s [%s %d-%02d-%02d]",getClass().getSimpleName(),this.nazwisko,this.dataUrodzenia.getYear(),this.dataUrodzenia.getMonthValue(),this.dataUrodzenia.getDayOfMonth());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Osoba3 osoba3 = (Osoba3) o;
        return Objects.equals(nazwisko, osoba3.nazwisko) &&
                Objects.equals(dataUrodzenia, osoba3.dataUrodzenia);
    }
    public int compareTo(Osoba3 o) {
        int result = this.getNazwisko().compareTo(o.getNazwisko());
        return result != 0 ? result : this.getDataUrodzenia().compareTo(o.getDataUrodzenia());
    }
    private String nazwisko;
    private LocalDate dataUrodzenia;
}
