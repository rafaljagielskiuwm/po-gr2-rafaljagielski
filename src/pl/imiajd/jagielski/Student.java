package pl.imiajd.jagielski;

public class Student extends Osoba {
    public Student(String nazwisko, int rokUrodzenia, String kierunek, double sredniaOcen) {
        super(nazwisko, rokUrodzenia);
        this.kierunek = kierunek;
        this.sredniaOcen=sredniaOcen;
    }
    public String getKierunek() {
        return this.kierunek;
    }

    public double getSredniaOcen() {
        return this.sredniaOcen;
    }

    @Override
    public String toString() {
        return String.format("%s, %s", super.toString(), this.kierunek);
    }
    private String kierunek;
    private double sredniaOcen;
}
