package pl.imiajd.jagielski;
import java.time.LocalDate;

class Student2 extends Osoba2
{
    public Student2(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, String kierunek, double sredniaOcen)
    {
        super(nazwisko,imiona, dataUrodzenia,plec);
        this.kierunek = kierunek;
        this.sredniaOcen=sredniaOcen;
    }

    public String getOpis()
    {
        return "kierunek studiów: " + kierunek;
    }
    public double getSredniaOcen(){
        return sredniaOcen;
    }

    private String kierunek;
    private double sredniaOcen;
}