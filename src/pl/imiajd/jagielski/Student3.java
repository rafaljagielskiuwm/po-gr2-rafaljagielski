package pl.imiajd.jagielski;

import java.time.LocalDate;

public class Student3 extends Osoba3 implements Cloneable, Comparable<Osoba3> {
    public Student3(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen) {
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }
    public double getSredniaOcen() { return this.sredniaOcen; }
    public int compareTo(Student3 o) {
        if(super.compareTo(o)!=0){
            return super.compareTo(o);
        }
        else {
            return Double.compare(this.getSredniaOcen(),o.getSredniaOcen());
        }
    }
    @Override
    public String toString() {
        return String.format("%s, srednia: %.2f", super.toString(),this.getSredniaOcen());
    }
    private double sredniaOcen;
}
