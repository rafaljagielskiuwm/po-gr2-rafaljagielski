package pl.imiajd.jagielski;
import java.time.LocalDate;

class Pracownik2 extends Osoba2
{
    public Pracownik2(String nazwisko,String[] imiona,LocalDate dataUrodzenia,boolean plec, double pobory, LocalDate dataZatrudnienia)
    {
        super(nazwisko, imiona, dataUrodzenia,plec);
        this.pobory = pobory;
        this.dataZatrudnienia=dataZatrudnienia;
    }

    public double getPobory()
    {
        return pobory;
    }
    public LocalDate getDataZatrudnienia(){
        return  dataZatrudnienia;
    }

    public String getOpis()
    {
        return String.format("pracownik z pensją %.2f zł", pobory);
    }

    private double pobory;
    private LocalDate dataZatrudnienia;
}